﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float changeDirection;
    public float directionNumber;
    public float movespeed;
    public bool leftrightrotation;
    public Rigidbody2D rigid;

    public bool startRight;


    public GameObject Dialogue;
    public Audio manage;
    public string sfx;


    private SpriteRenderer Flip;

    public Player player;


    public bool Yee;
    public bool Varanus;
    public bool Peter;
    public bool Trap;


    private void Awake()
    {
        // get a reference to the SpriteRenderer component on this gameObject
        Flip = GetComponent<SpriteRenderer>();
    }
    private void Start()
    {
        changeDirection = directionNumber;
        if (!startRight)
        {
            movespeed *= -1;
            Flip.flipX = true;
        }
        else
        {
            movespeed *= 1;
        }
    }

    void Update()
    {
        changeDirection -= Time.deltaTime;
        transform.Translate(Vector2.left * movespeed * Time.deltaTime);

        if (changeDirection <= 0)
        {
            movespeed *= -1;
            Flip.flipX = !Flip.flipX;
            leftrightrotation = !leftrightrotation;
            changeDirection = directionNumber;
        }

        /*if (changeDirection <= 0)
        {
            leftrightrotation = !leftrightrotation;
            changeDirection = directionNumber;
        }*/

        /*Vector2 movement = Vector2.zero;
        if (leftrightrotation)
        {
            movement.x -= movespeed;
            Flip.flipX = true;
        }
        if (!leftrightrotation)
        {
            movement.x += movespeed;
            Flip.flipX = false;
        }
        if (changeDirection < 0)
        {
            leftrightrotation = !leftrightrotation;
            changeDirection = directionNumber;
        }*/

        //rigid.MovePosition(transform.position + (Vector3)movement * Time.fixedDeltaTime);
        //rigid.MovePosition(transform.position + ((Vector3)movement * Time.deltaTime));
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            player.movespeed = 0;
            Dialogue.SetActive(true);
            manage.playsound(sfx);

            if (Yee)
            {
                player.EnemyHP = 3;
                player.EnemyDmg = 2;
            }
            else if (Varanus)
            {
                player.EnemyHP = 3;
                player.EnemyDmg = 3;
            }
            else if (Peter)
            {
                player.EnemyHP = 2;
                player.EnemyDmg = 0;
            }
            else if (Trap)
            {
                player.EnemyHP = 6;
                player.EnemyDmg = 3;
            }

            player.enemyHPBar.SetMaxHealth(player.EnemyHP);

            Destroy(gameObject);
            //Time.timeScale = 0;

        }

    }

}
