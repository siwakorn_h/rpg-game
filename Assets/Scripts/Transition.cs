﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

public class Transition : MonoBehaviour
{
    public UnityEvent OnTransition;
    public UnityEvent OutTransition;

    private string type;

    public void Trans()
    {
        if (type == "In")
        {
            OnTransition.Invoke();
        }
        else
        {
            OutTransition.Invoke();
        }
    }

    public void Settype(string value)
    {
        type = value;
    }



}
