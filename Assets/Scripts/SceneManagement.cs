﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class SceneManagement : MonoBehaviour
{
    public Animator startTran;
    public bool HellMode;

    public void resetscene()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    public void returntomainmenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void playgame()
    {
        if (!HellMode)
        {
            SceneManager.LoadScene("Gameplay");
        }
        else
        {
            SceneManager.LoadScene("GameHARD");
        }
    }

    public void HELLMODE()
    {
        HellMode = true;
    }

    public void Quitgame()
    {
        Application.Quit();
    }
    public void Start()
    {
        if (startTran != null)
        {
            startTran.Play("Slide out Trans");
        }
    }

}
