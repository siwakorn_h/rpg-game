﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGChange : MonoBehaviour
{
    public Animator PopUp;
    public string order;

    public Audio music;
    public string song;
    public GameObject spawnEnemy;
    public bool fade;

    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (!fade)
            {
                music.normalvolume();
                music.playmusic(song);
                spawnEnemy.SetActive(true);
                if (PopUp != null)
                {
                    PopUp.SetTrigger(order);
                }
            }
            else
            {
                music.fadesound();
            }
        }

    }
}
