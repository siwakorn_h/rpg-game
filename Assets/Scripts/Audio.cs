﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public List<AudioClip> audiolist = new List<AudioClip>();
    public AudioSource audiosource;
    public AudioSource audiomusic;
    public bool fade;
    public bool quickfade;
    public void playsound(string name)
    {
        foreach (AudioClip Clip in audiolist)
        {
            if (Clip.name.Equals(name))
            {
                audiosource.clip = Clip;
                //audiosource.PlayOneShot(Clip);
                audiosource.Play();
            }


        }
    }

    public void playmusic(string name)
    {
        fade = false;
        foreach (AudioClip Clip in audiolist)
        {
            if (Clip.name.Equals(name))
            {
                audiomusic.clip = Clip;
                audiomusic.Play();
            }


        }
    }
    public void playmusiconce(string name)
    {
        fade = false;
        foreach (AudioClip Clip in audiolist)
        {
            if (Clip.name.Equals(name))
            {
                audiomusic.clip = Clip;
                audiomusic.PlayOneShot(Clip);
            }


        }
    }

    public void stopmusic()
    {
        audiomusic.Stop();
    }
    public void lowvolume()
    {
        fade = false;
        audiomusic.volume = 0.35f;
    }
    public void normalvolume()
    {
        fade = false;
        audiomusic.volume = 0.6f;
    }
    public void fadesound()
    {
        fade = true;
    }
    public void stopsfx()
    {
        audiosource.volume = 0;
    }
    private void Update()
    {
        if (fade)
        {
            if (!quickfade)
            {
                audiomusic.volume -= Time.deltaTime / 2;
            }
            else
            {
                audiomusic.volume -= Time.deltaTime;
            }
        }
        if (audiomusic.volume == 0)
        {
            audiomusic.Stop();
            audiomusic.volume = 0.6f;
            fade = false;
        }
    }
}
