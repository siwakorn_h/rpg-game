﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
    public Slider slider;
    [SerializeField] private Image fill;
    [SerializeField] private Gradient grad;

    public void SetMaxHealth(int health)
    {
        slider.maxValue = health;
        slider.value = slider.maxValue;
    }

    public void SetHealth(int health)
    {
        slider.value = health;
        fill.color = grad.Evaluate(slider.normalizedValue);
    }
}
