﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TRaptor : MonoBehaviour
{
    public Player player;
    public GameObject enemysprite;
    public Audio manage;

    public GameObject dia1;
    public GameObject dia2;
    public GameObject dia3;
    public GameObject dia4;
    public GameObject dia5;
    public GameObject dia6;
    public GameObject dia7;
    public GameObject dia8;
    public GameObject dia9;
    public GameObject dia10;

    public GameObject redeye;
    public GameObject curiousIndicator;

    public bool enrage;
    public bool curious;


    public void EnrageHPCheck()
    {
        if (!enrage && player.EnemyHP != 0)
        {
            if (player.EnemyHP <= 3)
            {
                player.EnemyDmg *= 2;
                dia9.SetActive(true);
                redeye.SetActive(true);
                manage.playsound("RedEye");
                enrage = true;
            }
            else
            {
                dia2.SetActive(true);
            }
        }
        else
        {
            if (player.EnemyHP <= 0)
            {
                dia1.SetActive(true);
                enrage = false;
                redeye.SetActive(false);
                enemysprite.SetActive(false);
            }
            else
            {
                dia2.SetActive(true);
            }
        }
    
    }
    public void EnrageAttack()
    {
        if (!enrage)
        {
            dia3.SetActive(true);
        }
        else
        {
            dia4.SetActive(true);
        }
    
    }
    public void EnrageAppleCheck()
    {
        if (!enrage)
        {
            dia5.SetActive(true);
            curious = true;
            curiousIndicator.SetActive(true);
        }
        else
        {
            dia6.SetActive(true);
        }
    }
    public void CuriousAttackCheck()
    {
        if (!curious)
        {
            player.EnemyHP -= 1;
            dia7.SetActive(true);
        }
        else
        {
            player.EnemyHP -= 3;
            dia8.SetActive(true);
            curious = false;
            curiousIndicator.SetActive(false);
        }

    }
    public void HealCheck()
    {
        if (!curious)
        {
            dia2.SetActive(true);
        }
        else
        {
            dia10.SetActive(true);
        }
    }

}
