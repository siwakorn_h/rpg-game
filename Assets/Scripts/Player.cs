﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float movespeed = 8;
    public Rigidbody2D rigid;

    public TextMeshProUGUI HP;
    public TextMeshProUGUI appleamount;
    public TextMeshProUGUI dragonfruitamount;
    public TextMeshProUGUI appleamount2;
    public TextMeshProUGUI dragonfruitamount2;

    private SpriteRenderer Flip;
    public float apple;
    public float dragonfruit;
    public bool key;
    public float appleStacking;
    public GameObject stolenApple;

    public HPBar HPBar;
    public int maxHP = 20;
    public int currentHP;

    public HPBar enemyHPBar;
    public TextMeshProUGUI enemyHealth;
    public Audio music;
    public Collider2D Hitbox;
    public bool NoClip;

    public int PlayerDmg = 1;
    public int EnemyHP;
    public int EnemyDmg;
    public bool cheat;

    public GameObject EndingBad;
    public GameObject EndingGood;
    public GameObject PauseScreen;

    private void Awake()
    {
        // get a reference to the SpriteRenderer component on this gameObject
        Flip = GetComponent<SpriteRenderer>();
    }

    private void UIUpdater()
    {
        HP.text = currentHP.ToString();
        appleamount.text = apple.ToString();
        dragonfruitamount.text = dragonfruit.ToString();
        appleamount2.text = apple.ToString();
        dragonfruitamount2.text = dragonfruit.ToString();
        enemyHealth.text = EnemyHP.ToString();
    }


    public void DialoguePhase()
    {
        movespeed = 7;
        Time.timeScale = 1;
    }

    public void Attack()
    {
        enemyHPBar.SetHealth(EnemyHP -= PlayerDmg);
    }

    public void ReceiveDamage()
    {
        HPBar.SetHealth(currentHP -= EnemyDmg);
    }

    public void Heal()
    {
        HPBar.SetHealth(currentHP += maxHP / 4);
    }
    public void flipping()
    {
        Flip.flipX = true;
    }
    public void rewindtime()
    {
        transform.localPosition = new Vector2(-1.75f, -5.74f);
        Flip.flipX = true;
    }
    public void Ending()
    {
        if (cheat)
        {
            EndingBad.SetActive(true);
            //music.playmusic("Wind");
        }
        else
        {
            EndingGood.SetActive(true);
            music.playmusic("Ending");
        }
    }

    private void Start()
    {
        currentHP = maxHP;
        HPBar.SetMaxHealth(maxHP);

        enemyHPBar.SetMaxHealth(EnemyHP);
        key = false;
        Time.timeScale = 1;
        cheat = false;
}

    void Update()
    {
        UIUpdater();
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (movespeed != 0 && apple > 0)
            {
                apple -= 1;
                HPBar.SetHealth(currentHP += maxHP / 4);
                music.playsound("Eat");
            }
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            HPBar.SetHealth(currentHP += 1);
            cheat = true;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            apple += 1;
            cheat = true;
        }
        /*if (Input.GetKeyDown(KeyCode.I))
        {
            if (!NoClip)
            {
                Hitbox.enabled = false;
            }
            else
            {
                Hitbox.enabled = true;
            }
            NoClip = !NoClip;
        }*/



        Vector2 movement = Vector2.zero;
        if (Input.GetKey(KeyCode.W))
        {
            movement.y += movespeed;
        }
        if (Input.GetKey(KeyCode.S))
        {
            movement.y -= movespeed;
        }
        if (Input.GetKey(KeyCode.A))
        {
            movement.x -= movespeed;
            if (movespeed != 0)
            {
                Flip.flipX = true;
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
            movement.x += movespeed;
            if (movespeed != 0)
            {
                Flip.flipX = false;
            }
        }
            rigid.MovePosition(transform.position + (Vector3)movement * Time.fixedDeltaTime);

        if (currentHP > maxHP)
        {
            currentHP = maxHP;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 0;
            movespeed = 0;
            PauseScreen.SetActive(true);
        }

        enemyHPBar.SetHealth(EnemyHP);


        if (appleStacking > 0)
        {
            stolenApple.SetActive(true);
        }
        else
        {
            stolenApple.SetActive(false);
        }

    }
    /*void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        { 
            enemyHPBar.SetMaxHealth(EnemyHP);
        }

    }*/
}
