﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puppetor : MonoBehaviour
{
    public Player player;
    public Audio manage;

    public GameObject dia1;
    public GameObject dia2;

    public void AppleStealing()
    {
        if (player.apple > 0)
        {
            player.apple -= 1;
            player.appleStacking += 1;
            dia1.SetActive(true);
        }
        else
        {
            dia2.SetActive(true);
        }
    }
    public void AppleStack()
    {
        player.appleStacking += 1;
    }

}
