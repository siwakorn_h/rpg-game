﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Player player;
    public GameObject dialogue;
    public GameObject dialogue2;
    public DoorKey DoorKey;


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (player.key)
            {
                dialogue2.SetActive(true);
                player.key = false;
                player.movespeed = 0;
                DoorKey.keyUI.SetActive(false);
                Destroy(gameObject);
            }
            else
            {
                player.movespeed = 0;
                dialogue.SetActive(true);
            }
        }

    }

}
