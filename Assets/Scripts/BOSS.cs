﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BOSS : MonoBehaviour
{
    public Player player;
    public GameObject dia;
    public HPBar HPBar;
    public Audio manage;
    public string sfx;

    public GameObject dia1;
    public GameObject dia2;
    public GameObject dia3;
    public GameObject dia4;
    public GameObject dia5;

    public bool BossFight;
    public bool enrage;
    public bool activeOnce;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            dia.SetActive(true);
            player.movespeed = 0;
            player.EnemyHP = 1000;
            player.EnemyDmg = 16;
            player.enemyHPBar.SetMaxHealth(player.EnemyHP);
        }

    }

    public void DragonfruitConsume()
    {
        HPBar.SetMaxHealth(player.maxHP = 1000);
        HPBar.SetHealth(player.currentHP += 1000);
        player.PlayerDmg = 100;
        BossFight = true;
        player.dragonfruit -= 1;
    }

    public void EnrageAttack()
    {
        if (!BossFight)
        {
            dia1.SetActive(true);
        }
        else
        {
            dia2.SetActive(true);
            player.EnemyHP -= 100;
        }
    }

    public void EnrageReceive()
    {
        if (!enrage)
        {
            dia3.SetActive(true);
        }
        else
        {
            dia4.SetActive(true);
        }
    }

    public void EnrageCheck()
    {
        if (enrage)
        {
            if (activeOnce)
            {
                dia5.SetActive(true);
                activeOnce = false;
            }
        }
    }

    private void Update()
    {
        if (BossFight)
        {
            if (player.EnemyHP <= 600)
            {
                player.EnemyDmg = 200;
                enrage = true;
            }
        }
    }




}
