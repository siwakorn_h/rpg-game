﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NadoApple : MonoBehaviour
{
    public MiniBOSS BOSS;

    [SerializeField] private GameObject Apple1;
    [SerializeField] private GameObject Apple2;
    [SerializeField] private GameObject Apple3;
    [SerializeField] private GameObject Apple4;
    [SerializeField] private GameObject Apple5;

    void Update()
    {
        if (BOSS.AppleWant == 4)
        {
            Apple1.SetActive(true);
        }
        if (BOSS.AppleWant == 3)
        {
            Apple2.SetActive(true);
        }
        if (BOSS.AppleWant == 2)
        {
            Apple3.SetActive(true);
        }
        if (BOSS.AppleWant == 1)
        {
            Apple4.SetActive(true);
        }
        if (BOSS.AppleWant == 0)
        {
            Apple5.SetActive(true);
        }
    }
}
