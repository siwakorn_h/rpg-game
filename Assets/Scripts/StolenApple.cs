﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StolenApple : MonoBehaviour
{
    public GameObject dia;
    public Player player;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            dia.SetActive(true);
            player.apple += player.appleStacking;
            player.movespeed = 0;
            player.appleStacking = 0;
        }

    }


}
