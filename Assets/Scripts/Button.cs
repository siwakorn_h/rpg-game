﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public Player player;
    public GameObject enemysprite;
    public GameObject playersprite;
    public GameObject dia1;
    public GameObject dia2;
    public GameObject dia3;
    public GameObject dia4;
    public GameObject dia5;
    public GameObject dia6;
    public GameObject dia7;

    public Audio manage;
    public GameObject amount;

    public void EnemyHPCheck()
    {
        if (player.EnemyHP <= 0)
        {
            manage.fade = true;
            dia1.SetActive(true);
            enemysprite.SetActive(false);
        }
        else
        {
            dia2.SetActive(true);
        }
    }

    public void PlayerHPCheck()
    {
        if (player.currentHP > 0)
        {
            dia3.SetActive(true);
        }
        else
        {
            manage.fade = true;
            dia4.SetActive(true);
            playersprite.SetActive(false);
        }
    }

    public void EndBattle()
    {
        player.movespeed = 7;
    }


    public void AppleCheck()
    {
        if (player.apple > 0)
        {
            player.apple -= 1;
            dia5.SetActive(true);
            //dia7.SetActive(false);
            amount.SetActive(false);
        }
    }

    public void DragonfruitCheck()
    {
        if (player.dragonfruit > 0)
        {
            //player.dragonfruit -= 1;
            dia6.SetActive(true);
            dia7.SetActive(false);
            amount.SetActive(false);
        }
    }


}
