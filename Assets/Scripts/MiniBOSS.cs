﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniBOSS : MonoBehaviour
{
    public Player player;
    public GameObject EnemySprite;
    public GameObject dia;
    [SerializeField] private GameObject Apple;
    public int EatingTurn;
    public int AppleWant;
    public int Weaken;
    public Audio manage;

    public GameObject dia1;
    public GameObject dia2;
    public GameObject dia3;
    public GameObject dia4;
    public GameObject dia5;


    public bool GeneralYee;
    public bool Raptornado;
    public bool Velocipastor;
    public bool Velocipastor2;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            player.movespeed = 0;
            dia.SetActive(true);

            if (GeneralYee)
            {
                manage.playsound("Yee");
                player.EnemyHP = 6;
                player.EnemyDmg = 6;
            }
            else if (Raptornado)
            {
                manage.playsound("Gun");
                player.EnemyHP = 3;
                player.EnemyDmg = 8;
            }
            else if (Velocipastor)
            {
                player.EnemyHP = 8;
                player.EnemyDmg = 6;
            }
            else if (Velocipastor2)
            {
                player.EnemyHP = 8;
                player.EnemyDmg = 8;
            }
            player.enemyHPBar.SetMaxHealth(player.EnemyHP);
        }

    }


    //General Yee Vader (Mini BOSS 1) function
    public void StillEat()
    {
        if (player.EnemyHP > 0)
        {
            if (EatingTurn == 0)
            {
                dia1.SetActive(true);
            }
            else
            {
                dia2.SetActive(true);
            }
        }
        else
        {
            manage.fade = true;
            dia5.SetActive(true);
            EnemySprite.SetActive(false);
            Apple.SetActive(false);
        }
    }

    public void EatingCheck()
    {
        EatingTurn -= 1;
        if (EatingTurn == 0)
        {
            dia3.SetActive(true);
            Apple.SetActive(false);
        }
        else
        {
            dia4.SetActive(true);
        }
    }

    public void GiveApple()
    {
        EatingTurn = 2;
        Apple.SetActive(true);
    }

    private void Update()
    {
        if (EatingTurn > 0)
        {
            //Apple.SetActive(true);
        }
        else
        {
            EatingTurn = 0;
            //Apple.SetActive(false);
        }

        if (Weaken < 0)
        {
           // Weaken = 0;
        }

    }

    public void HealCheck()
    {
        if (EatingTurn > 0)
        {
            dia2.SetActive(true);
        }
        else
        {
            dia1.SetActive(true);
        }
    }


    public void Defeated()
    {
        Destroy(gameObject);
    }


    //Raptornado (Mini BOSS 2) function
    public void GiveApple2()
    {
        AppleWant -= 1;
    }

    public void AppleCheck()
    {
        if (AppleWant > 0)
        {
            dia1.SetActive(true);
        }
        else
        {
            dia2.SetActive(true);
        }
    }
    public void SelfDamage()
    {
        player.EnemyHP -= 5;
    }
    public void GetBackApple()
    {
        player.apple += 5;
    }

    //VelociPastor (Mini BOSS 3) function

    public void GiveApple3()
    {
        if (Weaken == 0)
        {
            player.EnemyDmg /= 2;
        }
        Weaken = 5;
        Apple.SetActive(true);
    }

    public void WeakenCheck()
    {
        if (player.EnemyHP == 0)
        {
            manage.fade = true;
            EnemySprite.SetActive(false);
            Apple.SetActive(false);
            dia5.SetActive(true);
            
        }
        if (Weaken != 0)
        {
            Weaken -= 1;
            if (Weaken > 0) //Weaken still going
            {
                dia1.SetActive(true);
            }
            else //Weaken is gone
            {
                dia2.SetActive(true);
                Apple.SetActive(false);
                player.EnemyDmg *= 2;
            }
        }
        else
        {
            dia1.SetActive(true);
        }
    }
    public void WeakenDamage()
    {
        if (Weaken == 0)
        {
            dia3.SetActive(true);
        }
        else
        {
            dia4.SetActive(true);
        }

    }
    public void HealCheck2()
    {
        if (Weaken != 0)
        {
            Weaken -= 1;
            if (Weaken > 0) //Weaken still going
            {
                dia1.SetActive(true);
            }
            else //Weaken is gone
            {
                dia2.SetActive(true);
                Apple.SetActive(false);
                player.EnemyDmg *= 2;
            }
        }
        else
        {
            dia1.SetActive(true);
        }
    }
    public void DragofruitPickUp()
    {
        player.dragonfruit++;
    }
}
