﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKey : MonoBehaviour
{
    public Player player;
    public GameObject Raptor;
    public GameObject keyUI;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            player.key = true;
            Raptor.SetActive(true);
            keyUI.SetActive(true);
            Destroy(gameObject);
        }

    }

}
